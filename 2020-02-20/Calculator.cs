﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _2020_02_20
{
    public class Calculator
    {
        string specialCharacers, negativeNumbers;
        public int Add(string numbers)
        {
            if (string.IsNullOrEmpty(numbers))
            {
                return 0;
            }

            if (!ContainsSpecialCharacters(numbers))
            {
                return Convert.ToInt32(numbers);
            }

            string[] numberWithoutDelimiter = numbers.Split(specialCharacers.ToArray());
            int[] numbersArray = GetNumbers(numberWithoutDelimiter);

            if (ContainsNegativeNumber(numbersArray))
            {
                throw new Exception("Negatives not allowed" + negativeNumbers.Remove(0, 1));
            }

            return ComputeSum(numbersArray);
        }

        public string ChangeMultipleCustomDelimitersToDefault(string numbers)
        {
            string firstLine = numbers.Substring(numbers.IndexOf("//") + 2, numbers.IndexOf("\n") - 2);
            firstLine = firstLine.Replace("][", ",").Replace("[", "").Replace("]", "");

            string[] delimiters = firstLine.Split(',');
            string secondLine = numbers.Substring(numbers.IndexOf("\n") + 1);

            for (int i = 0; i < delimiters.Length; i++)
            {
                secondLine = secondLine.Replace(delimiters[i], ",");
            }

            return secondLine;
        }

        public bool isBadString(string secondLine)
        {
            for (int i = 0; i < secondLine.Length; i++)
            {
                if (!char.IsDigit(secondLine[i]) && secondLine[i] != '-' && secondLine[i] != ',')
                {
                    return true;
                }
            }
            return false;
        }

        public bool ContainsSpecialCharacters(string numbers)
        {
            for (int i = 0; i < numbers.Length; i++)
            {
                if (!char.IsDigit(numbers[i]) && numbers[i] != '-')
                {
                    specialCharacers += numbers[i].ToString() + ",";
                }
            }

            return !string.IsNullOrEmpty(specialCharacers);
        }

        public int[] GetNumbers(string[] numberWithoutDelimiter)
        {
            string numbers = "";
            for (int i = 0; i < numberWithoutDelimiter.Length; i++)
            {
                var isNumeric = int.TryParse(numberWithoutDelimiter[i], out int num);
                if (isNumeric)
                {
                    numbers += "," + num;
                }
            }

            return numbers.Remove(0, 1).Split(',').Select(int.Parse).ToArray();
        }

        public bool ContainsNegativeNumber(int[] numbersArray)
        {
            for (int i = 0; i < numbersArray.Length; i++)
            {
                if (numbersArray[i] < 0)
                {
                    negativeNumbers += ", " + numbersArray[i];
                }
            }

            return !string.IsNullOrEmpty(negativeNumbers);
        }

        public int ComputeSum(int[] numbersArray)
        {
            int sum = 0;
            for (int i = 0; i < numbersArray.Length; i++)
            {
                if (numbersArray[i] < 1001)
                {
                    sum += numbersArray[i];
                }
            }
            return sum;
        }
    }
}
